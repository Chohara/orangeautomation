package pagelibrary;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage {
	WebDriver driver;
	public LoginPage(WebDriver driver) {
		this.driver=driver;
	}
	
	 
	@FindBy(xpath="//input[@id='txtUsername']")
	WebElement username;
	@FindBy(xpath="//input[@id='txtPassword']")
   WebElement pwd;
	@FindBy(xpath="//input[@id='btnLogin']")
	WebElement loginbutton;
	
	@FindBy(id="divLogo")
   WebElement hrmgImage;
	public boolean isImageExit() {
		return hrmgImage.isDisplayed();
		
		
	}
	@FindBy(xpath="//span[text()='Invalid credentials']")
	WebElement errorMsg;
public boolean isErrorMsgExits() {
		
		return errorMsg.isDisplayed();
	}
	public void loginIntotheApplication(String userName,String password) {
		username.sendKeys(userName);
		pwd.sendKeys(password);
		loginbutton.click();
	}
	
}
