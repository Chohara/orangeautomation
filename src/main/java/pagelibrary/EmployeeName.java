package pagelibrary;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class EmployeeName {
  
	WebDriver driver;
	
	@FindBy(name="assignleave[txtEmployee][empName]")
   WebElement employeename;
	public void EmployeeName(WebDriver driver) {
		this.driver=driver;
		
	}
	
	public void writeEmployeeName(String name) {
		employeename.sendKeys(name);
		
	}
	
}
