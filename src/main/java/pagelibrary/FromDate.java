package pagelibrary;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import utilities.CommonUtilities;

public class FromDate {
	WebDriver driver;
	public void FromData(WebDriver driver) {
		this.driver=driver;
	}
   @FindBy(name="assignleave[txtFromDate]")
   WebElement fdate;
   
   public void text() {
	   fdate.click();
   }
 @FindBy(xpath="//div[@id='ui-datepicker-div']/div/div/select[1]")
  WebElement selectmonth;
  public void monthDropdown(String value) {
	  CommonUtilities.selectValueDropDown(selectmonth, value);
  }
@FindBy(xpath="//div[@id='ui-datepicker-div']/div/div/select[2]")
  WebElement selectyear;
  public void yearDropdown(String value) {
	  CommonUtilities.selectValueDropDown(selectyear,value);
  }
 // @FindBy(xpath="//a[@class='ui-state-default ui-state-active ui-state-hover']")
 @FindBy(xpath="//div[@id='ui-datepicker-div']/table/tbody/tr[4]/td[3]/a")
  WebElement selectdate;
 public void dateontext() {
	selectdate.click();
  }
}
