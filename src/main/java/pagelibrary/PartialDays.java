package pagelibrary;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import utilities.CommonUtilities;

public class PartialDays {
	WebDriver driver;
  public void PartialDays() {
  this.driver=driver;
	}
  @FindBy(name="assignleave[partialDays]")
  WebElement selectdays;
  public void selectPartialDays(String value) {
	  CommonUtilities.selectValueDropDown(selectdays, value);
  }
}