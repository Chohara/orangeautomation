package pagelibrary;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import utilities.CommonUtilities;

public class PageDropDown {
	WebDriver driver;
	
  @FindBy(name="assignleave[txtLeaveType]")
	  WebElement leaveDropDown;
  public PageDropDown(WebDriver driver) {
	  this.driver=driver;
  }
  
  public void selectDropDown(String value) {
	 CommonUtilities.selectValueDropDown(leaveDropDown,value);
  }
  }

