package driverbase;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;

import io.github.bonigarcia.wdm.WebDriverManager;
import utilities.CommonUtilities;

public class TestBase {
	
	public Properties prop;
	public static WebDriver driver;
	
	@BeforeSuite
	public void readProperties() throws Exception {
		prop=new Properties();
		FileInputStream fs=new FileInputStream("C:\\Users\\vibha\\java_home\\TestingProject\\src\\main\\java\\properties\\config.properties");
	 prop.load(fs);
	 
}
	@BeforeTest
	public void browserLanuch() {
		
		if(prop.get("browser").equals("chrome")){
			
	
		WebDriverManager.chromedriver().setup();
		driver=new ChromeDriver();
		
		}
		else if(prop.get("browser").equals("firefox")){
			WebDriverManager.firefoxdriver().setup();
			driver=new FirefoxDriver();
			
		}
		else {
			System.out.println("Please specify the correct browser");
			
		}
		driver.get(prop.getProperty("appUrl"));
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	}
	@AfterMethod
	public void takesScreenShotForFailureTest(ITestResult result) throws IOException {
		
		if(ITestResult.FAILURE==result.getStatus()) {
			CommonUtilities.takesScreenShot(result.getName());
		}
		
	}

}

