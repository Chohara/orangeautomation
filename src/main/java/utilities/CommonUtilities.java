package utilities;


import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import driverbase.TestBase;

public class CommonUtilities extends TestBase {
	


	public static void takesScreenShot(String testName) throws IOException {
		
		TakesScreenshot ts=(TakesScreenshot)driver;
	File src=ts.getScreenshotAs(OutputType.FILE);
	FileUtils.copyFile(src, new File("C:\\Users\\vibha\\java_home\\TestingProject\\screenshots\\"+testName+".png"));
	}
	public static void selectValueDropDown(WebElement ele,String value) {
		 Select sel=new Select(ele);
		 sel.selectByVisibleText(value);
	}
  
	   
   }

