package testcases;


import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

import driverbase.TestBase;
import pagelibrary.DashBoard;
import pagelibrary.EmployeeName;
import pagelibrary.FromDate;
import pagelibrary.LoginPage;
import pagelibrary.PageDropDown;
import pagelibrary.PartialDays;

public class LoginTest extends TestBase{
	LoginPage loginpage;
	DashBoard dashboardpage;
	@Test(priority=0)
	public void verifyLoginFunctionality() {
	loginpage=PageFactory.initElements(driver, LoginPage.class)	;
	
	boolean result=loginpage.isImageExit();
	Assert.assertTrue(result,"this image is exit or not");
	
	loginpage.loginIntotheApplication(prop.getProperty("username"),prop.getProperty("pwd"));
	}

 @Test(priority=1)
  public void verifyDashBoardPage() {
  dashboardpage=PageFactory.initElements(driver, DashBoard.class);
  
  String txt=dashboardpage.getTextDashboard();
  Assert.assertEquals("Dashboard",txt);
 
  dashboardpage.verifyAssignLeave();
 String expURL="https://opensource-demo.orangehrmlive.com/index.php/leave/assignLeave";
  Assert.assertEquals(expURL,driver.getCurrentUrl());
}

 @Test(priority=2)
 public void verifyPageDropDown() {
	PageDropDown pagedropdown = PageFactory.initElements(driver, PageDropDown.class) ;
	pagedropdown.selectDropDown("CAN - FMLA");
	
	pagedropdown.selectDropDown("CAN - Vacation");
 }
@Test(priority=3)
public void validateEmployeeName(){
	
	EmployeeName employeename=PageFactory.initElements(driver, EmployeeName.class) ;
	
	employeename.writeEmployeeName("jack");
	
}
@Test(priority=4)
public void verifyFromDate() {
	FromDate fromdate=PageFactory.initElements(driver, FromDate.class);
	fromdate.text();
	fromdate.monthDropdown("Jul");
			
	fromdate.yearDropdown("2022");
	fromdate.dateontext();
	
}
//@Test(priority=5)
public void verifyPartialDays() {
	PartialDays partialdays=PageFactory.initElements(driver,PartialDays.class);
	partialdays.selectPartialDays("All Days");
}
}
 
 
 
 
 
