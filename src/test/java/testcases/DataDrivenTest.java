package testcases;

import java.io.IOException;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import utilities.ExcelUtilities;

public class DataDrivenTest {
	
	//@DataProvider(name = "data")
	public Object[][] getData(String sheetName) throws IOException{
		//String sheetName="Sheet1";
	String filePath="C:\\Users\\vibha\\java_home\\TestingProject\\src\\main\\java\\testdata\\data.xlsx";
  ExcelUtilities ex=new ExcelUtilities(filePath);
	int rowNum=ex.getRowCount(sheetName);
	int columnCount=ex.getColumnCount(sheetName);
	Object obj[][]=new Object[rowNum][columnCount];
	
	for(int i=1;i<=rowNum;i++)
		for(int j=0;j<columnCount;j++) {
			obj[i-1][j]=ex.getCellData("Sheet1",i, j);
		}
	return obj;
	}
@DataProvider(name="data")
 public Object[][] getCellData() throws IOException{
	Object obj[][]=getData("Sheet1");
			return obj;
}



@Test(dataProvider="data")
public void test(String userName,String pwd) {
	System.out.println(userName);
	System.out.println(pwd);
}

}

