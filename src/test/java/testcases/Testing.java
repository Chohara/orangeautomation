package testcases;

import java.io.IOException;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import driverbase.TestBase;
import pagelibrary.LoginPage;

public class Testing extends TestBase{
	LoginPage loginPage;
	
	
	@DataProvider(name="data")
	 public Object[][] getCellData() throws IOException{
		Object obj[][]=new DataDrivenTest().getData("Sheet1");
				return obj;

}
	  @Test(dataProvider="data")
	  public void verifyLoginFunctionality(String userName,String pwd) {
		  loginPage = PageFactory.initElements(driver,LoginPage.class);
		loginPage. loginIntotheApplication(userName,pwd);
		if(userName.equals(userName)&pwd.equals(pwd)) {
		System.out.println("Succesfully logged in ");
		}
		else 
		
		
			Assert.assertTrue(loginPage.isErrorMsgExits());
		
		}
}